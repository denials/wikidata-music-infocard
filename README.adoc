= Demonstration of Wikidata-powered infocards =

Copyright: Dan Scott (dan@coffeecode.net), Laurentian University

License: GPL version 2 or later

This is a simple implementation of infocards powered by Wikidata. To adapt this
to your music catalogue, you may be able to simply change the five CSS selector
variables in wikidata_music_card.js to match your catalogue's structure.

The code on which this implementation is based is running in production on an
Evergreen library system at Laurentian University at https://laurentian.concat.ca/.

== Limitations ==

* Currently limited to search and display in English.
* Currently limited to the domain of music.  If you want to use this in a
  broader domain, you will need to change the SPARQL query accordingly.
  Consider using lookups based on authority IDs--for example, you could
  use Wikidata IDs in MARC $1 subfields and look up the data directly, or
  use existing authority IDs (such as LC) to find matches based on Wikidata's
  extensive authority control data.


